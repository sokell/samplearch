﻿using TOPT.DataServices.Model;
using TOPT.DataServices.Repository.Common;
using TOPT.DataServices.Service.Common;

namespace TOPT.DataServices.Service
{
    public class ExternalDataService : GenericExternalDataService<ExternalData>, IExternalDataService
    {
        private readonly IGenericExternalDataRepository<ExternalData> _repository;

        public ExternalDataService()
        {
            _repository = new GenericExternalDataRepository<ExternalData>();
        }

        protected ExternalDataService(IGenericExternalDataRepository<ExternalData> repository) : base(repository)
        {
            _repository = repository;
        }
        //add- here -anything 'custom' - not in GenericExternalDataService

    }
}
