﻿using TOPT.DataServices.Model;
using TOPT.DataServices.Service.Common;

namespace TOPT.DataServices.Service
{
    public interface IExternalDataService : IGenericExternalDataService<ExternalData>
    {
        //add- here -anything 'custom' - not in IGenericExternalDataService
    }
}
