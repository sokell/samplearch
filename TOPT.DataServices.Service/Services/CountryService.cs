﻿using Infrastructure.Logging;
using TOPT.DataServices.Model;
using TOPT.DataServices.Repository;
using TOPT.DataServices.Repository.Common;
using TOPT.DataServices.Service.Common;

namespace TOPT.DataServices.Service
{
    public class CountryService : EntityService<Country>, ICountryService
    {
        //private IUnitOfWork _unitOfWork;
        //private readonly ICountryRepository _countryRepository;
        
        public CountryService(ICountryRepository countryRepository)
            : base(countryRepository)
        {
            //_unitOfWork = unitOfWork;
            //_countryRepository = countryRepository;
        }
    }
}
