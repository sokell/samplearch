﻿using Infrastructure.Logging;
using TOPT.DataServices.Model;
using TOPT.DataServices.Repository;
using TOPT.DataServices.Repository.Common;
using TOPT.DataServices.Service.Common;

namespace TOPT.DataServices.Service
{
    public class CountryService : EntityService<Country>, ICountryService
    {
        public CountryService(ICountryRepository countryRepository)
            : base(countryRepository)
        {
        
        }
    }
}
