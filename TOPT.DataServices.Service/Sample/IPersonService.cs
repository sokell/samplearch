﻿using TOPT.DataServices.Model;
using TOPT.DataServices.Service.Common;

namespace TOPT.DataServices.Service
{
    public interface IPersonService : IEntityService<Person>
    {

    }
}
