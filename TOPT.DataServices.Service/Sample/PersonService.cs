﻿using TOPT.DataServices.Model;
using TOPT.DataServices.Repository;
using TOPT.DataServices.Repository.Common;
using TOPT.DataServices.Service.Common;

namespace TOPT.DataServices.Service
{
    public class PersonService : EntityService<Person>, IPersonService
    {
        public PersonService(IPersonRepository personRepository)
            : base(personRepository)
        {
            
        }
    }
}
