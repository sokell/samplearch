﻿using TOPT.DataServices.Model.Common;

namespace TOPT.DataServices.Service.Common
{
    /// <summary>
    /// IEntityService
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <seealso cref="TOPT.DataServices.Service.Common.IService" />
    public interface IGenericExternalDataService<TEntity> : IService
     where TEntity : BaseEntity
    {
        /// <summary>
        /// Size of data
        /// </summary>
        /// <value>
        /// The count.
        /// </value>
        int Size(string data);

        /// <summary>
        /// Gets the data from socket.
        /// </summary>
        /// <returns>The data from socket.</returns>
        /// <param name="endpoint">Endpoint.</param>
        string GetDataFromSocket<T>(string endpoint, T data) where T : BaseEntity, new();
    }
}
