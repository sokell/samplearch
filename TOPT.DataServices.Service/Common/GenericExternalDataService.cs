﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Infrastructure.Logging;
using TOPT.DataServices.Model;
using TOPT.DataServices.Model.Common;
using TOPT.DataServices.Repository.Common;

namespace TOPT.DataServices.Service.Common
{
    /// <summary>
    /// EntityService
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <seealso cref="TOPT.DataServices.Service.Common.IGenericExternalDataService{TEntity}" />
    public abstract class GenericExternalDataService<TEntity> : IGenericExternalDataService<TEntity> where TEntity : ExternalData
    {
        /// <summary>
        /// The _repository
        /// </summary>
        private readonly IGenericExternalDataRepository<TEntity> _repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericExternalDataService{TEntity}" /> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        protected GenericExternalDataService(IGenericExternalDataRepository<TEntity> repository)
        {
            _repository = repository;
        }

        protected GenericExternalDataService()
        {
            _repository = new GenericExternalDataRepository<TEntity>();
        }

        /// <summary>
        /// Size
        /// </summary>
        /// <value>
        /// The Size
        /// </value>
        public int Size(string data){
            return data.Length;
        }

        /// <summary>
        /// Gets the data from socket.
        /// </summary>
        /// <returns>The data from socket.</returns>
        /// <param name="endpoint">Endpoint.</param>
        public string GetDataFromSocket<T>(string endpoint, T data) where T : BaseEntity, new()
        {
            return _repository.GetDataFromSocket(endpoint, data);
        }

        public static async Task ClientRequestDownload(string endpointUrl, CancellationToken cancellationToken)
        {
            var result = string.Empty;

            if (string.IsNullOrWhiteSpace(endpointUrl))
                return;

            await Task.Factory.StartNew(() =>
            {
                Audit.Log.Debug("Task started ...\r\n");

                using (var wc = new WebClient())
                {
                    // Create an event handler to receive the result.
                    // Check status of WebClient, not external token.
                    wc.DownloadStringCompleted += (obj, e)
                     =>
                    {
                        Audit.Log.Debug(!e.Cancelled ?
                            "The download has completed:\r\n" + e.Result :
                            "The download was canceled.");
                    };

                    // Do not initiate download if the external token
                    // has already been canceled.
                    if (cancellationToken.IsCancellationRequested) return;

                    // Register the callback to a method that can unblock.
                    using (cancellationToken.Register(() => wc.CancelAsync()))
                    {
                        Audit.Log.Debug("Starting request\r\n");
                        wc.DownloadStringAsync(new Uri(endpointUrl));
                    }
                }

            }, cancellationToken).ConfigureAwait(true);

            Audit.Log.Debug("Returning from function\r\n");
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="EntityService{TEntity}" /> class.
        /// </summary>
        ~GenericExternalDataService()
        {
            Dispose(false);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        private void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);

            Audit.Log.Info($"Dispose :: CurrentDbContext Entity {typeof (TEntity).DeclaringType} destroyed");
        }


        /// <summary>
        /// Disposes all external resources.
        /// </summary>
        /// <param name="disposing">The dispose indicator.</param>
        private void Dispose(bool disposing)
        {
            if (!disposing) return;

            //TODO: do any needed cleanup

            Dispose();
        }
    }
}
