﻿# TOPT-DataServices

#####

## <b>TOPT WebApi REST Web Data Services Solution<b/>


### Solution Description - 

``` 
* Host data, exposed as REST Web Services
* Data input from external sockets
* Parsed or Mapped and Deserialized as JSON/XML 
* Added reference libraries for data delivery capabilities, with this formats:

	>> JSON
	>> XML
	>> KML
	>> GML
	>> WKT (Well Known Text)
	>> GeoJson
	>> and CZML for Cesium

```

#####
