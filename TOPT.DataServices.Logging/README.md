﻿## Development - Infrastructure Logging

![](http://localhost:9000/api/project_badges/measure?project=infrastructure.logging&metric=alert_status)
![](https://img.shields.io/jenkins/s/https/jenkins.qa.ubuntu.com/view/Precise/view/All%20Precise/job/precise-desktop-amd64_default.svg)

Logging - log4js for logging

## Security

npm run scan will perform a local security scan with SonarQube - if DiSA - server-side only available; update the sonar-project-properties file to point to 'https://sonarqube.di2e.net/' - this will then run on the server.

## Changelog

Keep track of major version changes with input, keep with check-in.

## Usage

The client or end-user of this logging project will need to do the following:
 * add log4net NuGet package, Tools --> Manage NuGet Packages ...
 * add reference to Infrastructure.Logging project
 * add ConfigSection entry: 
 -- Example: <configSections>
    <section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler, log4net"/>

 * add appSettings key: 
 -- Example: <appSettings>
        <add key="log4net.Config" value="AllTheSame.Log.config"/>

 * add log4net config section:
 
 -- Example: <log4net>
    <root>
      <level value="ALL"/>
      <appender-ref ref="RollingFileAppender"/>
      <appender-ref ref="ConsoleAppender"/>
      <appender-ref ref="TraceAppender"/>
    </root>
    <appender name="RollingFileAppender" type="log4net.Appender.RollingFileAppender,log4net">
      <file value="../Logs/AllTheSame.WebApi.log"/>
      <appendToFile value="true"/>
      <rollingStyle value="Size"/>
      <maxSizeRollBackups value="5"/>
      <maximumFileSize value="10MB"/>
      <staticLogFileName value="true"/>
      <!--<filter type="log4net.Filter.StringMatchFilter">
        <stringToMatch value="test"/>
      </filter>
      <filter type="log4net.Filter.StringMatchFilter">
        <stringToMatch value="error"/>
      </filter>
      <filter type="log4net.Filter.DenyAllFilter"/>-->
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%date [%thread] %level %logger - %message%newline%exception"/>
      </layout>
    </appender>
    <appender name="ConsoleAppender" type="log4net.Appender.ConsoleAppender">
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%date{ABSOLUTE} [%thread] %level %logger - %message%newlineExtra Info: %property{testProperty}%newline%exception"/>
      </layout>
      <filter type="log4net.Filter.LevelRangeFilter">
        <levelMin value="INFO"/>
        <levelMax value="FATAL"/>
      </filter>
    </appender>
    <appender name="TraceAppender" type="log4net.Appender.TraceAppender">
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%d [%t] %-5p %c %m%n"/>
      </layout>
    </appender>
  </log4net>

  ##
