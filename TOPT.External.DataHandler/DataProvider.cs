﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TOPT.DataServices.Model;

namespace TOPT.External.DataHandler
{
    public enum ResponseType 
    {
        JSON,
        XML,
        KML,
        GML,
        CZML
    }

    public class DataProvider
    {
        public dynamic Get(ResponseType responseType)
        {
            //this is just a brainstorm example
            var result = default(string);
            var entity = new ExternalData
            {
                Data = "This is Data"
            };
            switch (responseType)
            {
                case ResponseType.JSON:
                    result = ExternalDataHandler.GetDataFromSocket<ExternalData>("localhost", entity);
                    break;
                case ResponseType.XML:
                    result = ExternalDataHandler.GetDataFromSocket<ExternalData>("localhost", entity); 
                    break;
                case ResponseType.KML:
                    //reference added:

                    var data = ExternalDataHandler.GetDataFromSocket<ExternalData>("localhost", entity); 
                    //var kml = new SharpKml.Base.Parser().Parse([read dat with stream reader]) ... etc; 
                    //
                    //SEE: https://github.com/googlegis/sharpkml/tree/master/Examples
                    //
                    var kml = DataFormatter.ParseKml();//TOFO: !!! this is just a test of Kml parser - need to update

                    result = (dynamic)kml;
                    break;
                case ResponseType.GML:
                    //reference added:
                    //SEE: https://github.com/Terradue/DotNetGeoJson
                    //OR
                    //SEE: https://github.com/NetTopologySuite/NetTopologySuite
                    break;
                case ResponseType.CZML:
                    //reference added:
                    //SEE: https://github.com/AnalyticalGraphicsInc/czml-writer/tree/master/DotNet
                    break;
                default:
                    break;
            }
            return result;
        }
    }
}
