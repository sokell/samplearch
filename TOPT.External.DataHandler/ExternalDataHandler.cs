﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TOPT.DataServices.Model;
using TOPT.DataServices.Model.Common;
using TOPT.DataServices.Utilities;
using TOPT.DataServices.Utilities.Cache;

namespace TOPT.External.DataHandler
{
    public class ExternalDataHandler 
    {
        protected ExternalDataHandler()
        {
            //
        }

        //get data from client socket
        public static string GetDataFromSocket<T>(string endpoint, T data) where T: BaseEntity, new()//TODO: this will be similar to GenericRepository - but for our data input types, instead of Db results from EF
        {
            string result;

            //var obj = AppUtility.GetObjectFromJson<ExternalData>(data);


            result = AsyncClientSocket.GetClientSocketData<T>(endpoint, data);

            ////cache handling - gget it from cache, or gget data from source, then cache it
            ////OF course, caching with Redis is dependent upon approval to use Redis
            //var clientCache = new RedisCacheProvider();
            //if (!clientCache.IsInCache("clientData.Id"))
            //{
            //    result = AsyncClientSocket.GetClientSocketData<string>(endpoint);
            //    clientCache.Set("clientData.Id", result);
            //    return result;
            //}
            //result = clientCache.Get<string>("clientData.Id");
            return result;
        }
    }
}
