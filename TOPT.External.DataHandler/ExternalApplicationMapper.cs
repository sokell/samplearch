﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TOPT.External.DataHandler
{
#pragma warning disable S3400 // Methods should not return constants
    public class ExternalApplicationMapper
    {
        protected ExternalApplicationMapper() { } //basic singleton

        //This class is meant to be where TVBridge, JMPS-P, etc. are called - return data for mapping and formatter within ExternalDataHandler
        public static string GetTvBridgeData() => "TvBridge_Data";

        public static string GetJmpsPData() => "JMPS-P_Data";

        //etc ...
    }
#pragma warning restore S3400 // Methods should not return constants
}
