﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using Infrastructure.Logging;
using SharpKml.Base;
using SharpKml.Dom;

#pragma warning disable S3400 // Methods should not return constants
namespace TOPT.External.DataHandler
{
    public static class DataFormatter
    {
        /// <summary>
        /// !!! TODO: This is only an Exmple - NEEDS modification
        /// Parses a string (using the default Kml namespace) and converts it to Kml objects.
        /// </summary>
        const string InputKml =
                "<Placemark>" +
                    "<name>hi</name>" +
                "</Placemark>";

        public static Kml ParseKml()
        {
            Audit.Log.Info($"{MethodBase.GetCurrentMethod()} :: Parsing '{InputKml}'...");

            Parser parser = new Parser();
            parser.ParseString(InputKml, false);

            Placemark placemark = (Placemark)parser.Root;
            Audit.Log.Info($"{MethodBase.GetCurrentMethod()} :: The placemark name is '{placemark.Name}'.");
            Audit.Log.Info($"{MethodBase.GetCurrentMethod()} :: Creating a point at 37.42052549 latitude and -122.0816695 longitude.\n");

            // This will be used for the placemark
            Point point = new Point
            {
                Coordinate = new Vector(37.42052549, -122.0816695)
            };

            placemark.Name = "Cool Statue";
            placemark.Geometry = point;

            // This is the root element of the file
            Kml kml = new Kml
            {
                Feature = placemark
            };

            Serializer serializer = new Serializer();
            serializer.Serialize(kml);
            Audit.Log.Info($"{MethodBase.GetCurrentMethod()} :: {serializer.Xml}");


            Audit.Log.Info($"{MethodBase.GetCurrentMethod()} :: \nReading Xml...");

            parser.ParseString(serializer.Xml, true);

            kml = (Kml)parser.Root;
            placemark = (Placemark)kml.Feature;
            point = (Point)placemark.Geometry;

            Audit.Log.Info($"{MethodBase.GetCurrentMethod()} :: Latitude:{point.Coordinate.Latitude} Longitude:{point.Coordinate.Longitude}");

            return kml;
        }


        //parse raw XML -- TODO: this needs to return the final data still - ref, out, result - something
        public static void ParseXml(dynamic parent, XElement node)
        {
            Audit.Log.Info($"{MethodBase.GetCurrentMethod()} :: called ...");

            if (node.HasElements)
            {
                if (node.Elements(node.Elements().First().Name.LocalName).Count() > 1)
                {
                    //list
                    var item = new ExpandoObject();
                    var list = new List<dynamic>();
                    foreach (var element in node.Elements())
                    {
                        ParseXml(list, element);
                    }

                    AddProperty(item, node.Elements().First().Name.LocalName, list);
                    AddProperty(parent, node.Name.ToString(), item);
                }
                else
                {
                    var item = new ExpandoObject();

                    foreach (var attribute in node.Attributes())
                    {
                        AddProperty(item, attribute.Name.ToString(), attribute.Value.Trim());
                    }

                    //element
                    foreach (var element in node.Elements())
                    {
                        ParseXml(item, element);
                    }

                    AddProperty(parent, node.Name.ToString(), item);
                }
            }
            else
            {
                AddProperty(parent, node.Name.ToString(), node.Value.Trim());
            }
        }

        private static void AddProperty(dynamic parent, string name, object value)
        {
            if (parent is List<dynamic>)
            {
                (parent as List<dynamic>).Add(value);
            }
            else
            {
                (parent as IDictionary<String, object>)[name] = value;
            }
        }
    }
}
#pragma warning restore S3400 // Methods should not return constants