﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TOPT.DataServices.Model.Common;
using Infrastructure.Logging;

namespace TOPT.DataServices.Model
{
#pragma warning disable S101 // Types should be named in camel case
    /// <summary>
    /// DataContext
    /// </summary>
    public class TOPTDataServicesContext : DbContext
#pragma warning restore S101 // Types should be named in camel case
    {

        public TOPTDataServicesContext()
            : base("Name=TOPTDataServicesContext")
        {
            //
            Configuration.LazyLoadingEnabled = false;
        }

        #region List of Entity Sets
        //
        
#pragma warning disable S125 // Sections of code should not be "commented out"
//public DbSet<Person> Persons { get; set; }
        //
        #endregion List of Entity Sets

        public override int SaveChanges()
#pragma warning restore S125 // Sections of code should not be "commented out"
        {
            var result = default(int);

            var modifiedEntries = ChangeTracker.Entries()
                .Where(x => x.Entity is IAuditableEntity
                    && (x.State == EntityState.Added || x.State == EntityState.Modified));

            foreach (var entry in modifiedEntries)
            {
                var entity = entry.Entity as IAuditableEntity;
                if (entity == null) continue;
                if (Thread.CurrentPrincipal == null) continue;

                var identityName ="";
                try
                {
                    identityName = Thread.CurrentPrincipal.Identity.Name;
                }
                catch (SecurityException ex)
                {
                    Audit.Log.Error("{0}", ex);
                }

                var now = DateTime.UtcNow;
                if (entry.State == EntityState.Added)
                {
                    entity.CreatedBy = identityName;
                    entity.CreatedDate = now;
                }
                else {
                    base.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
                    base.Entry(entity).Property(x => x.CreatedDate).IsModified = false;                   
                }

                entity.UpdatedBy = identityName;
                entity.UpdatedDate = now;
            }

            try
            {
                result = base.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                Audit.Log.Error("{0}", ex);
            }
            catch (DbEntityValidationException ex)
            {
                Audit.Log.Error("{0}", ex);
            }
            catch (NotSupportedException ex)
            {
                Audit.Log.Error("{0}", ex);
            }
            catch (ObjectDisposedException ex)
            {
                Audit.Log.Error("{0}", ex);
            }
            catch (InvalidOperationException ex)
            {
                Audit.Log.Error("{0}", ex);
            }

            return result;
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            var result = default(Task<int>);

            var modifiedEntries = ChangeTracker.Entries()
                .Where(x => x.Entity is IAuditableEntity
                    && (x.State == EntityState.Added || x.State == EntityState.Modified));

            foreach (var entry in modifiedEntries)
            {
                var entity = entry.Entity as IAuditableEntity;
                if (entity == null) continue;
                if (Thread.CurrentPrincipal == null) continue;

                var identityName = "";
                try
                {
                    identityName = Thread.CurrentPrincipal.Identity.Name;
                }
                catch (SecurityException ex)
                {
                    Audit.Log.Error("{0}", ex);
                }

                var now = DateTime.UtcNow;
                if (entry.State == EntityState.Added)
                {
                    entity.CreatedBy = identityName;
                    entity.CreatedDate = now;
                }
                else
                {
                    base.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
                    base.Entry(entity).Property(x => x.CreatedDate).IsModified = false;
                }

                entity.UpdatedBy = identityName;
                entity.UpdatedDate = now;
            }

            try
            {
                result = base.SaveChangesAsync(cancellationToken);
            }
            catch (DbUpdateException ex)
            {
                Audit.Log.Error("{0}", ex);
            }
            catch (DbEntityValidationException ex)
            {
                Audit.Log.Error("{0}", ex);
            }
            catch (NotSupportedException ex)
            {
                Audit.Log.Error("{0}", ex);
            }
            catch (ObjectDisposedException ex)
            {
                Audit.Log.Error("{0}", ex);
            }
            catch (InvalidOperationException ex)
            {
                Audit.Log.Error("{0}", ex);
            }

            return result;
        }
    }    
}
