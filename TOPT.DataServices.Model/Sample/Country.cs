﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using TOPT.DataServices.Model.Common;

namespace TOPT.DataServices.Model
{
    public class Country : Entity<long>
    {      
        [MinLength(2)]
        [MaxLength(150)]
        [Display(Name="Country Name")]
        public string Name { get; set; }
        
        public virtual IEnumerable<Person> Persons { get; set; }
    }
}
