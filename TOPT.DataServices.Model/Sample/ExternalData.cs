﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using TOPT.DataServices.Model.Common;

namespace TOPT.DataServices.Model
{
    [Table("ExternalData")]
    public class ExternalData : AuditableEntity<long>
    {   
        public string Name { get; set; }

        public string Data { get; set; }
    }
}
