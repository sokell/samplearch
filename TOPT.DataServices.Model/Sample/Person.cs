﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using TOPT.DataServices.Model.Common;

namespace TOPT.DataServices.Model
{
    [Table("Person")]
    public class Person : AuditableEntity<long>
    {   
        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(20)]
        public string Phone { get; set; }

        [MaxLength(100)]
        public string Address { get; set; }

        [MaxLength(50)]
        public string State { get; set; }

        [Display(Name="Country")]
        public long CountryId { get; set;  }

        //!!!!!!!!
        //This becomes required for valid data-binding
        [ForeignKey("CountryId")]
        public virtual Country Country { get; set; }
        
    }
}
