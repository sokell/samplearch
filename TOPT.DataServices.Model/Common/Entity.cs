﻿namespace TOPT.DataServices.Model.Common
{
    public abstract class BaseEntity
    {
        protected BaseEntity()
        {
        }
    }

    public abstract class Entity<T> : BaseEntity, IEntity<T> 
    {
        protected Entity()
        {
        }

        public virtual T Id { get; set; }
    }

    public abstract class Entity : IEntity<long>//default
    {
        protected Entity()
        {
        }

        protected Entity(long id) => Id = id;

        public long Id { get; set; }
    }
}
