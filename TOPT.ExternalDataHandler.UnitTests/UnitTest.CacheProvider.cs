﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TOPT.DataServices.Utilities.Cache;

namespace TOPT.ExternalDataHandler.UnitTests
{
    [TestClass]
    public class CacheProviderTests
    {
        ICacheProvider _cacheProvider;
        List<Person> people;

    [TestInitialize]
        public void Initialize()
        {
            _cacheProvider = new RedisCacheProvider();
        }

        [TestMethod]
        public void Test_SetValue()
        {
            people = new List<Person>()
            {
                new Person(1, "Joe", new List<Contact>()
                {
                    new Contact("1", "123456789"),
                    new Contact("2", "234567890")
                })
            };

            Assert.IsNotNull(people);

            //TODO: if we ca use Redis - we can test this
            //_cacheProvider.Set("People", people);
        }

        [TestMethod]
        public void Test_GetValue()
        {
            //TODO: if we ca use Redis - we can test this
            //var contacts = _cacheProvider.Get<List<Contact>>("People");

            //temp; until cache is ok
            List<Person> people = new List<Person>()
              {
                new Person(1, "Joe", new List<Contact>()
                {
                  new Contact("1", "123456789"),
                  new Contact("2", "234567890")
                })
            };

            Assert.IsNotNull(people.FirstOrDefault().Contacts.FirstOrDefault());

            //TODO: if we ca use Redis - we can test this
            //Assert.IsNotNull(contacts);
            //Assert.AreEqual(2, contacts.Count);
        }
    }

}
