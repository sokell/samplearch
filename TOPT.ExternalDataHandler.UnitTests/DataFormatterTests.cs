﻿using Infrastructure.Logging;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using TOPT.External.DataHandler;

/*
 * This class is here, as an attempt to setup testing with NUnit3
 * If no benefit, from NUnit3 - it will go away
 * 
 * Additionaly - Unit-Tests should move to a separate Test-Project
 * 
 * */

#pragma warning disable S125 // Sections of code should not be "commented out"
namespace TOPT.ExternalDataHandler.UnitTests
{
    [TestFixture]
    internal class DataFormatterTestAttribute : Attribute
    {
        public DataFormatterTestAttribute()
        {
        }

        [Test]
        public void TestFormatXmlFromString()
        {
            //read from a string value
            const string xmlInput = "<?xml version=\"1.0\" encoding=\"utf­8\" ?><contacts><contact id=\"1\"><firstName>Michael</firstName><lastName>Jordan</lastName><age>40</age><dob>1965</dob><salary>100.35</salary></contact><firstName>Scottie</firstName><lastName>Pippen</lastName><age>38</age><dob>1967</dob><salary>55.28</salary></contact></contacts>";

            //var xDoc = XDocument.Parse(xmlInput, LoadOptions.None);
            //TODO: use a textreader or such

            Assert.IsNotNull(xmlInput);

            dynamic root = new ExpandoObject();

            //CallDataFormatter(root, xDoc);
            Audit.Log.Info($"{MethodBase.GetCurrentMethod()} :: SampleDataXML [XDocumet]: {root}");
        }

        [Test]
        public void TestFormatXmlFromFile()
        {
            var currentPath = AppDomain.CurrentDomain.BaseDirectory;
            //read from file
            var xDoc = XDocument.Load(new StreamReader($"{currentPath}/Data/SampleDataXml.xml"));

            dynamic root = new ExpandoObject();

            //CallDataFormatter(root, xDoc);
            Audit.Log.Info($"{MethodBase.GetCurrentMethod()} :: SampleDataXML [XDocumet]: {root}");

            Assert.IsNotNull(xDoc);
        }

        //[Test]
        //public void TestFormatXmlFromWebRequest()
        //{
        //    //read from url
        //    const string requestUriString = @"http://localhost:3000/xmldata"; //todo: build a super lite web service to host sample data

        //    var request = WebRequest.Create(requestUriString) as HttpWebRequest;
        //    request.Credentials = CredentialCache.DefaultNetworkCredentials;
        //    var xDoc = XDocument.Load(request.GetResponse().GetResponseStream());

        //    dynamic root = new ExpandoObject();

        //    CallDataFormatter(root, xDoc);
        //}

        public void CallDataFormatter(ExpandoObject root, XDocument xDocument)
        {
            DataFormatter.ParseXml(root, xDocument.Elements().First());

            //Console.WriteLine(root.contacts.contact.Count);
            //Console.WriteLine(root.contacts.contact[0].firstName);
            //Console.WriteLine(root.contacts.contact[0].id);

            XElement xElement = root.First() as dynamic;

            Audit.Log.Info($"{MethodBase.GetCurrentMethod()} :: root [XElement]: {xElement}");

            XElement _ = ConvertToXml(xElement);

            Audit.Log.Info($"{MethodBase.GetCurrentMethod()} :: ConvertToXml [XElement]: {_}");

            //result.Parent.Name.Equals("");
            //result.Parent.Value.Equals("");
            //etc

            Assert.IsNotNull(root);

            // Actual vs Expected tests here
            var x = 1;
            var y = 1;
            Assert.IsTrue(x == y);
        }

        public static dynamic ConvertToXml(XElement parent)
        {
            dynamic output = new ExpandoObject();

            output.Name = parent.Name.LocalName;
            output.Value = parent.Value;

            output.HasAttributes = parent.HasAttributes;
            if (parent.HasAttributes)
            {
                output.Attributes = new List();
                foreach (XAttribute attr in parent.Attributes())
                {
                    var tempKeyVal = new KeyValuePair<string, string>(attr.Name.LocalName, attr.Value);
                    output.Attributes.Add(tempKeyVal);
                }
            }

            output.HasElements = parent.HasElements;
            if (parent.HasElements)
            {
                output.Children = new List();
                foreach (XElement element in parent.Elements())
                {
                    dynamic temp = ConvertToXml(element);
                    output.Elements.Add(temp);
                }
            }

            return output;
        }
    }
}
#pragma warning restore S125 // Sections of code should not be "commented out"