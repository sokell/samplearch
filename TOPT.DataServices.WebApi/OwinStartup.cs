﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using FluentValidation.WebApi;
using Microsoft.Owin;
using Owin;
using Autofac.Integration.WebApi;
using Infrastructure.Logging;
using Microsoft.Owin.Cors;

[assembly: OwinStartup(typeof(TOPT.DataServices.WebApi.OwinStartup))]

namespace TOPT.DataServices.WebApi
{
    /// <summary>
    /// OwinStartup
    /// </summary>
    public class OwinStartup
    {
        /// <summary>
        /// Configurations the specified application.
        /// </summary>
        /// <param name="app">The application.</param>
        public void Configuration(IAppBuilder app)
        {
            var builder = new ContainerBuilder();

            // STANDARD WEB API SETUP:

            // Get your HttpConfiguration. In OWIN, you'll create one
            // rather than using GlobalConfiguration.
            var config = new HttpConfiguration();

            //// Register your Web API controllers.
            
            //// Run other optional steps, like registering filters,
            //// per-controller-type services, etc., then set the dependency resolver
            //// to be Autofac.
            ////var container = builder.Build();
            ////config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            ////Autofac Configuration
            ////var builder = new Autofac.ContainerBuilder();
            
            // Register your Web API controllers.
            //builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            

            // OPTIONAL: Register the Autofac filter provider.
            //builder.RegisterWebApiFilterProvider(config);
            config.EnsureInitialized();

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            
            //config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            //Audit.Log.Debug("Application_Start :: DependencyResolver called");
            
            // OWIN WEB API SETUP:

            // Register the Autofac middleware FIRST, then the Autofac Web API middleware,
            // and finally the standard Web API middleware.
            app.UseAutofacMiddleware(container);

            app.UseAutofacWebApi(config);

            app.UseWebApi(config);
            
            FluentValidationModelValidatorProvider.Configure(config);


        }
    }
}
