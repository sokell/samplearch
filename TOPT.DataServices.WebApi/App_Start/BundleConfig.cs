﻿using System.Web.Optimization;

namespace TOPT.DataServices.WebApi
{
    /// <summary>
    /// BundleConfig
    /// </summary>
    public class BundleConfig
    {
        /// <summary>
        /// BundleConfig
        /// </summary>
        protected BundleConfig()
        {
        }

        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        /// <summary>
        /// Registers the bundles.
        /// </summary>
        /// <param name="bundles">The bundles.</param>
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = true;
        }
    }
}
