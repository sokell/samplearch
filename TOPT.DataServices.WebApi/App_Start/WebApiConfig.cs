﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using TOPT.DataServices.Repository.Common;
using TOPT.DataServices.WebApi.Modules;
using TOPT.DataServices.Service.Common;
using TOPT.DataServices.Service;
using System;
using TOPT.DataServices.Model.Common;

namespace TOPT.DataServices.WebApi
{
    /// <summary>
    /// WebApiConfig
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// Registers the specified configuration.
        /// </summary>
        /// <param name="config">The configuration.</param>
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            //config.SuppressDefaultHostAuthentication(); //breaks current swagger

            //config.Filters.Add(new HostAuthenticationFilter(OAuth...));

            // Web API routes
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(

                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            var builder = new ContainerBuilder();

            builder.RegisterModule(new RepositoryModule());
            builder.RegisterModule(new ServiceModule());
            builder.RegisterModule(new EfModule());

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            
            //builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
            //builder.RegisterType<RulesProcessor>().As<IRulesProcessor>().InstancePerRequest();

            builder.RegisterType<FluentValidation.Validators.AbstractComparisonValidator>().As<FluentValidation.Validators.IComparisonValidator>().InstancePerRequest();

            //builder.RegisterAssemblyTypes(typeof(EntityService<>).Assembly)
            //       .Where(t => (t.Name.EndsWith("Service") ))
            //.AsImplementedInterfaces().InstancePerRequest();

            //builder.RegisterAssemblyTypes(typeof(EntityService<>).Assembly)
                   //.Where(t => (t.Name.EndsWith("Service") && !t.Name.EndsWith("DataService")) )
                   //.AsImplementedInterfaces().InstancePerRequest();

            //builder.RegisterAssemblyTypes(typeof(GenericExternalDataService<>).Assembly)
                   //.Where(t => t.Name.EndsWith("DataService"))
                   //.AsImplementedInterfaces().InstancePerRequest();

            IContainer container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}
