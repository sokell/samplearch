﻿using System.Data.Entity;
using Autofac;
using TOPT.DataServices.Model;
using TOPT.DataServices.Repository;
using TOPT.DataServices.Repository.Common;
using TOPT.DataServices.Service;
using TOPT.DataServices.Service.Common;

namespace TOPT.DataServices.WebApi.Modules
{

    /// <summary>
    /// EfModule
    /// </summary>
    /// <seealso cref="Autofac.Module" />
    public class EfModule : Autofac.Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule(new RepositoryModule());
            builder.RegisterType(TOPTDataServicesContext()).As(typeof(DbContext)).InstancePerLifetimeScope();
            //builder.RegisterType(implementationType: typeof(UnitOfWork)).As(services: typeof(IUnitOfWork)).InstancePerRequest();
        }

        private static System.Type TOPTDataServicesContext() => typeof(TOPTDataServicesContext);
    }
}