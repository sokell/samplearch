﻿using TOPT.DataServices.Model;
using TOPT.DataServices.Service;
using TOPT.DataServices.WebApi.Controllers.Base;

namespace TOPT.DataServices.WebApi.Controllers
{
    /// <summary>
    /// CountryController using base controller
    /// </summary>
    /// <seealso cref="BaseExternalApiController{ExternalData}" />
    /// <seealso cref="System.Web.Http.ApiController" />
    public class ExternalDataController : BaseExternalApiController<ExternalData>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExternalDataController" /> class.
        /// </summary>
        /// <param name="service">The service.</param>
        public ExternalDataController(IExternalDataService service) : base(service)
        {
            
        }
    }
}