﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TOPT.DataServices.Model;
using TOPT.DataServices.Model.Common;
using TOPT.DataServices.Service.Common;
using TOPT.DataServices.Utilities;

namespace TOPT.DataServices.WebApi.Controllers.Base
{
    /// <summary>
    /// BaseApiController{T} - base controller for CRUD
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class BaseExternalApiController<T> : ApiController where T : BaseEntity, IEntity<long>, new()
    {
        #region Local Properties / Fields
        //
        /// <summary>
        /// The _cancellation token source
        /// </summary>
        private readonly CancellationTokenSource _cancellationTokenSource;

        /// <summary>
        /// The _cancellation token
        /// </summary>
        private readonly CancellationToken _cancellationToken;

        /// <summary>
        /// The _service
        /// </summary>
        private readonly IGenericExternalDataService<T> _service;
        //
        #endregion Local Properties / Fields

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseExternalApiController{T}" /> class.
        /// </summary>
        /// <param name="service">The service.</param>
        public BaseExternalApiController(IGenericExternalDataService<T> service)
        {
            _service = service;

            _cancellationTokenSource = new CancellationTokenSource();
            _cancellationToken = _cancellationTokenSource.Token;
        }

        #region CRUD 
        //

        // POST: api/{T}
        /// <summary>
        /// Posts the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="endpoint"></param>
        /// <returns>
        /// {T}
        /// </returns>
        [ResponseType(typeof(IHttpActionResult))]
        public virtual async Task<IHttpActionResult> Post(T entity, string @endpoint)
        {
            if (!ModelState.IsValid)
            {
                _cancellationTokenSource.Cancel();
                return BadRequest(ModelState);
            }

            //var data = AppUtility.GetJsonFromObject<T>(entity);

            //var response = Request.CreateResponse<T>(HttpStatusCode.Created, entity);

            //entity = await _service.AddAsync(entity, _cancellationToken);
            var result = _service.GetDataFromSocket(endpoint, entity);

            return CreatedAtRoute("DefaultApi", new { id = entity.Id }, entity);
        }

        //
        #endregion CRUD 

        /// <summary>
        /// Releases the unmanaged resources that are used by the object and, optionally, releases the managed resources.
        /// </summary>
        /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //any resources created
                if(_cancellationTokenSource != null)
                {
                    _cancellationTokenSource.Dispose();
                }
            }
            base.Dispose(disposing);
        }
    }
}