﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using TOPT.DataServices.Model;
using TOPT.DataServices.Model.Common;
using TOPT.DataServices.Service;
using TOPT.DataServices.Service.Common;
using TOPT.DataServices.WebApi.Controllers.Base;

namespace TOPT.DataServices.WebApi.Controllers
{
    public class ExternalDataController_old : ApiController
    {
        #region Local Properties / Fields
        //
        /// <summary>
        /// The _cancellation token source
        /// </summary>
        private readonly CancellationTokenSource _cancellationTokenSource;

        /// <summary>
        /// The _cancellation token
        /// </summary>
        private readonly CancellationToken _cancellationToken;

        /// <summary>
        /// The _service
        /// </summary>
        private readonly IGenericExternalDataService<ExternalData> _service;
        //
        #endregion Local Properties / Fields

        //(IEntityService<T> service)
        /// <summary>
        /// Initializes a new instance of the <see cref="ExternalDataController<typeparamref name="ExternalData"/>>" /> class.
        /// </summary>
        /// <param name="service">The service.</param>
        public ExternalDataController_old(IGenericExternalDataService<ExternalData> service) 
        {
            _service = service;
        }

        public long Id { get; set; }

        // POST: api/{T}
        /// <summary>
        /// Posts the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// {T}
        /// </returns>
        [ResponseType(typeof(IHttpActionResult))]
        public virtual async Task<IHttpActionResult> Post(string endpoint, ExternalData entity)
        {
            if (!ModelState.IsValid)
            {
                _cancellationTokenSource.Cancel();
                return BadRequest(ModelState);
            }

            //entity = await _service.AddAsync(entity, _cancellationToken);
            var result = _service.GetDataFromSocket(endpoint);

            return CreatedAtRoute("DefaultApi", new { id = entity.Id }, entity);
        }
    }
}
