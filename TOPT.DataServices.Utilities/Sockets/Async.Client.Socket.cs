﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using Infrastructure.Logging;
using System.Reflection;
using TOPT.DataServices.Model;
using TOPT.DataServices.Model.Common;

namespace TOPT.DataServices.Utilities
{
    // State object for receiving data from remote device.  
    public class StateObject
    {
        // Client socket.  
        public Socket WorkSocket { get; set; } = null;
        // Size of receive buffer.  
        public const int BufferSize = 256;
        
        // Receive buffer.  
        public byte[] Buffer { get; set; } = new byte[BufferSize];
        public StringBuilder Sb { get; set; } = new StringBuilder();

        //Cstr
        public StateObject()
        {
        }
        public StateObject(Socket workSocket, byte[] buffer, StringBuilder sb)
        {
            WorkSocket = workSocket ?? throw new ArgumentNullException(nameof(workSocket));
            Buffer = buffer ?? throw new ArgumentNullException(nameof(buffer));
            Sb = sb ?? throw new ArgumentNullException(nameof(sb));
        }
    }

    public class AsyncClientSocket
    {
        protected AsyncClientSocket() { }

        // The port number for the remote device.  
        private const string host = "localhost";
        private const int port = 9000;

        // ManualResetEvent instances signal completion.  
        private static ManualResetEvent connectDone =
            new ManualResetEvent(false);
        private static ManualResetEvent sendDone =
            new ManualResetEvent(false);
        private static ManualResetEvent receiveDone =
            new ManualResetEvent(false);

        // The response from the remote device.  
        private static dynamic response;

        private static void StartClient<T>(string endpoint, T data) where T: BaseEntity, new()
        {
            // Connect to a remote device.  
            try
            {
                // Establish the remote endpoint for the socket.  
                // The name of the   
                // remote device is "localhhost:????".  
                IPHostEntry ipHostInfo = Dns.GetHostEntry(endpoint);//TODO: what is the port? Do we need to parse the port?
                IPAddress ipAddress = ipHostInfo.AddressList[0];
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);
                Audit.Log.Info($"{MethodBase.GetCurrentMethod()} :: TCP/IP socket created ...");

                // Create a TCP/IP socket.  
                Socket client = new Socket(ipAddress.AddressFamily,
                    SocketType.Stream, ProtocolType.Tcp);
                Audit.Log.Info($"{MethodBase.GetCurrentMethod()} :: socket remote endpoint: {remoteEP}");

                // Connect to the remote endpoint.  
                client.BeginConnect(remoteEP,
                    new AsyncCallback(ConnectCallback), client);
                connectDone.WaitOne();

                var d = data as ExternalData;


                // Send test data to the remote device.  
                Send(client, d.Data);//TODO: replace text or remove
                sendDone.WaitOne();

                // Receive the response from the remote device.  
                Receive(client);
                receiveDone.WaitOne();
                Audit.Log.Info($"{MethodBase.GetCurrentMethod()} :: request for response call - complete");

                // Write the response to the loggger.  
                Audit.Log.Debug($"{MethodBase.GetCurrentMethod()} :: Response received : {response}");

                // Release the socket.  
                client.Shutdown(SocketShutdown.Both);
                client.Close();
                Audit.Log.Info($"{MethodBase.GetCurrentMethod()} :: socket released and closed");
            }
            catch (Exception e)
            {
                Audit.Log.Error($"Exception: {MethodBase.GetCurrentMethod()}", e);
            }
        }

        private static void ConnectCallback(IAsyncResult ar)
        {
            Audit.Log.Info($"{MethodBase.GetCurrentMethod()} :: called ...");
            try
            {
                // Retrieve the socket from the state object.  
                Socket client = (Socket)ar.AsyncState;

                // Complete the connection.  
                client.EndConnect(ar);

                Audit.Log.Debug($"{MethodBase.GetCurrentMethod()} :: Socket connected to {client?.RemoteEndPoint?.ToString()}");

                // Signal that the connection has been made.  
                connectDone.Set();
            }
            catch (Exception e)
            {
                Audit.Log.Error(e.ToString());
            }
        }

        private static void Receive(Socket client)
        {
            Audit.Log.Info($"{MethodBase.GetCurrentMethod()} :: called ...");
            try
            {
                // Create the state object.  
                StateObject state = new StateObject
                {
                    WorkSocket = client
                };

                // Begin receiving the data from the remote device.  
                client.BeginReceive(state.Buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReceiveCallback), state);

                Audit.Log.Info($"{MethodBase.GetCurrentMethod()} :: Begin receiving the data from the remote device call - complete");
            }
            catch (Exception e)
            {
                Audit.Log.Error($"Exception: {MethodBase.GetCurrentMethod()}", e);
            }
        }

        private static void ReceiveCallback(IAsyncResult ar)
        {
            Audit.Log.Info($"{MethodBase.GetCurrentMethod()} :: called ...");
            try
            {
                // Retrieve the state object and the client socket   
                // from the asynchronous state object.  
                StateObject state = (StateObject)ar.AsyncState;
                Socket client = state.WorkSocket;

                // Read data from the remote device.  
                int bytesRead = client.EndReceive(ar);

                Audit.Log.Info($"{MethodBase.GetCurrentMethod()} :: reading data from remote device, length: {bytesRead}");

                if (bytesRead > 0)
                {
                    // There might be more data, so store the data received so far.  
                    state.Sb.Append(Encoding.ASCII.GetString(state.Buffer, 0, bytesRead));

                    // Get the rest of the data.  
                    client.BeginReceive(state.Buffer, 0, StateObject.BufferSize, 0,
                        new AsyncCallback(ReceiveCallback), state);
                }
                else
                {
                    // All the data has arrived; put it in response.  
                    if (state.Sb.Length > 1)
                    {
                        response = state.Sb.ToString();
                    }
                    Audit.Log.Info($"{MethodBase.GetCurrentMethod()} :: all data read, saved to response");

                    // Signal that all bytes have been received.  
                    receiveDone.Set();
                }
            }
            catch (Exception e)
            {
                Audit.Log.Error($"Exception: {MethodBase.GetCurrentMethod()}", e);
            }
        }

        private static void Send(Socket client, String data)
        {
            Audit.Log.Info($"{MethodBase.GetCurrentMethod()} :: called ...");

            // Convert the string data to byte data using ASCII encoding.  
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.  
            client.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), client);
        }

        private static void SendCallback(IAsyncResult ar)
        {
            Audit.Log.Info($"{MethodBase.GetCurrentMethod()} :: called ...");
            try
            {
                // Retrieve the socket from the state object.  
                Socket client = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.  
                int bytesSent = client.EndSend(ar);
                Audit.Log.Debug($"{MethodBase.GetCurrentMethod()} :: Sent {bytesSent} bytes to server.");

                // Signal that all bytes have been sent.  
                sendDone.Set();
            }
            catch (Exception e)
            {
                Audit.Log.Error($"Exception: {MethodBase.GetCurrentMethod()}", e);
            }
        }

        public static string GetClientSocketData<T>(string endpoint, T data) where T: BaseEntity, new()
        {
            Audit.Log.Info($"{MethodBase.GetCurrentMethod()} :: called ... Endpoint: {endpoint}");

            StartClient(endpoint, data);

            Audit.Log.Info($"{MethodBase.GetCurrentMethod()} :: call complete ... Endpoint: {endpoint}");

            return response;//TODO: response should be populated here, but is it? Async done?
        }
    }
}