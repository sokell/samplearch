﻿using System;
using System.Diagnostics;
using System.Linq.Expressions;

namespace TOPT.DataServices.Common.Extensions
{
    public static partial class GlobalExtensions
    {
#pragma warning disable S1144 // Unused private types or members should be removed
                             /// <summary>
                             ///     Guard
                             /// </summary>
        internal static class Guard
        {
            /// <summary>
            ///     Greaters the than.
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="minimum">The minimum.</param>
            /// <param name="value">The value.</param>
            /// <param name="reference">The reference.</param>
            internal static void GreaterThan<T>(T minimum, T value, Expression<Func<T>> reference)
                where T : IComparable<T>
            {
                if (value.CompareTo(minimum) > 0)
                {
                    return;
                }

                Throw.GuardFailureNotGreaterThan(GetName(reference), value, minimum);
            }

            /// <summary>
            ///     Throws an exception if the value is less than or equal to the minimum.
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="minimum">The minimum.</param>
            /// <param name="value">The value.</param>
            /// <param name="name">The name.</param>
            [DebuggerStepThrough]
            internal static void GreaterThan<T>(T minimum, T value, string name) where T : IComparable<T>
            {
                if (value.CompareTo(minimum) > 0)
                {
                    return;
                }

                Throw.GuardFailureNotGreaterThan(name, value, minimum);
            }

            /// <summary>
            ///     Throws an exception if the value is less than the minimum.
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="minimum">The minimum.</param>
            /// <param name="value">The value.</param>
            /// <param name="reference">The reference.</param>
            [DebuggerStepThrough]
            internal static void GreaterThanOrEqualTo<T>(T minimum, T value, Expression<Func<T>> reference)
                where T : IComparable<T>
            {
                if (value.CompareTo(minimum) >= 0)
                {
                    return;
                }

                Throw.GuardFailureNotGreaterThanOrEqualTo(GetName(reference), value, minimum);
            }

            /// <summary>
            ///     Throws an exception if the value is less than the minimum.
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="minimum">The minimum.</param>
            /// <param name="value">The value.</param>
            /// <param name="name">The name.</param>
            [DebuggerStepThrough]
            internal static void GreaterThanOrEqualTo<T>(T minimum, T value, string name) where T : IComparable<T>
            {
                if (value.CompareTo(minimum) >= 0)
                {
                    return;
                }

                Throw.GuardFailureNotGreaterThanOrEqualTo(name, value, minimum);
            }

            /// <summary>
            ///     Throws an exception if the value is greater than or equal to the maximum.
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="maximum">The maximum.</param>
            /// <param name="value">The value.</param>
            /// <param name="reference">The reference.</param>
            [DebuggerStepThrough]
            internal static void LessThan<T>(T maximum, T value, Expression<Func<T>> reference) where T : IComparable<T>
            {
                if (value.CompareTo(maximum) < 0)
                {
                    return;
                }

                Throw.GuardFailureNotLessThan(GetName(reference), value, maximum);
            }

            /// <summary>
            ///     Throws an exception if the value is greater than or equal to the maximum.
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="maximum">The maximum.</param>
            /// <param name="value">The value.</param>
            /// <param name="name">The name.</param>
            [DebuggerStepThrough]
            internal static void LessThan<T>(T maximum, T value, string name) where T : IComparable<T>
            {
                if (value.CompareTo(maximum) < 0)
                {
                    return;
                }

                Throw.GuardFailureNotLessThan(name, value, maximum);
            }

            /// <summary>
            ///     Throws an exception if the value is greater than the maximum.
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="maximum">The maximum.</param>
            /// <param name="value">The value.</param>
            /// <param name="reference">The reference.</param>
            [DebuggerStepThrough]
            internal static void LessThanOrEqualTo<T>(T maximum, T value, Expression<Func<T>> reference)
                where T : IComparable<T>
            {
                if (value.CompareTo(maximum) <= 0)
                {
                    return;
                }

                Throw.GuardFailureNotLessThanOrEqualTo(GetName(reference), value, maximum);
            }

            /// <summary>
            ///     Throws an exception if the value is greater than the maximum.
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="maximum">The maximum.</param>
            /// <param name="value">The value.</param>
            /// <param name="name">The name.</param>
            [DebuggerStepThrough]
            internal static void LessThanOrEqualTo<T>(T maximum, T value, string name) where T : IComparable<T>
            {
                if (value.CompareTo(maximum) <= 0)
                {
                    return;
                }

                Throw.GuardFailureNotLessThanOrEqualTo(name, value, maximum);
            }

            /// <summary>
            ///     Throws an exception if the value is null.
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="value">The value.</param>
            /// <param name="reference">The reference.</param>
            [DebuggerStepThrough]
            internal static void IsNotNull<T>(T value, Expression<Func<T>> reference)
            {
                if (!IsNullableType(typeof(T)))
                {
                    return;
                }
                if (value == null)
                {
                    Throw.ArgumentNullException(GetName(reference));
                }
            }

            /// <summary>
            ///     Throws an exception if the value is null.
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="value">The value.</param>
            /// <param name="name">The name.</param>
            [DebuggerStepThrough]
            internal static void IsNotNull<T>(T value, string name)
            {
                if (!IsNullableType(typeof(T)))
                {
                    return;
                }
                if (value == null)
                {
                    Throw.ArgumentNullException(name);
                }
            }

            /// <summary>
            ///     Gets the name.
            /// </summary>
            /// <param name="reference">The reference.</param>
            /// <returns></returns>
            private static string GetName(Expression reference)
            {
                var lambda = reference as LambdaExpression;
                var member = lambda?.Body as MemberExpression;
                return member?.Member.Name;
            }
        }
#pragma warning restore S1144 // Unused private types or members should be removed
    }
}
