﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StackExchange.Redis.Extensions.Core;
using StackExchange.Redis.Extensions.Core.Configuration;
using StackExchange.Redis.Extensions.Newtonsoft;

namespace TOPT.DataServices.Utilities.Cache
{
    public class RedisCacheProvider : ICacheProvider
    {
        private readonly NewtonsoftSerializer _serializer;
        private readonly RedisConfiguration _configuration;//TODO: pull from app.config or set in class 

        public RedisCacheProvider()
        {
            _serializer = new NewtonsoftSerializer();
            _configuration = new RedisConfiguration();
        }

        public RedisCacheProvider(NewtonsoftSerializer serializer, RedisConfiguration configuration)
        {
            _serializer = serializer;
            _configuration = configuration;
        }

        public void Set<T>(string key, T value)
        {
            this.Set(key, value, TimeSpan.Zero);
        }

        public void Set<T>(string key, T value, TimeSpan timeout)
        {
            using (var cacheClient = new StackExchangeRedisCacheClient(_serializer, _configuration))
            {
                cacheClient.Add(key, value, DateTimeOffset.Now.AddMinutes(10));
            }
        }

        public T Get<T>(string key)
        {
            T result = default(T);

            using (var cacheClient = new StackExchangeRedisCacheClient(_serializer, _configuration))
            {
                result = cacheClient.Get<T>(key);
            }

            return result;
        }

        public bool Remove(string key)
        {
            bool removed = false;

            using (var cacheClient = new StackExchangeRedisCacheClient(_serializer, _configuration))
            {
                removed = cacheClient.Remove(key);
            }

            return removed;
        }

        public bool IsInCache(string key)
        {
            bool isInCache = false;

            using (var cacheClient = new StackExchangeRedisCacheClient(_serializer, _configuration))
            {
                isInCache = cacheClient.Exists(key);
            }

            return isInCache;
        }
    }
}
