﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Schema;
using NUnit.Framework;
using static TOPT.DataServices.Utilities.XML.XmlHelper;

namespace TOPT.DataServices.Utilities.XML.Sample
{
    public class ParseFootballClubXml
    {
        protected ParseFootballClubXml()
        {
        }

        //for an AutoMapper approach - see: https://github.com/DannyDouglass/AutoMapperXmlMappingDemo

        private const string XmlFile = @"..\..\FootbalClub.xml";
        private const string XmlSchemaFile = @"..\..\FootballClub.xsd";

        public static void Parse()
        {
            if (!Validate(XmlFile, XmlSchemaFile))
                return;

            var xmlHelper = new XmlSerialisationHelper<FootballClub>();

            var stJohnstone = xmlHelper.Deserialise(XmlFile);

            //System.Diagnostics.Debug.WriteLine() // <-- safe

            Console.WriteLine("Team Name:\t\t{0}", stJohnstone.Name);
            Console.WriteLine("Manager Name:\t\t{0}", stJohnstone.Manager.Name);
            Console.WriteLine("First Stadium Stand:\t{0}", stJohnstone.Stadium.Stands.First());
            Console.WriteLine("Number of Players:\t{0}", stJohnstone.Players.Count);

            Console.ReadKey();
        }

        private static bool Validate(string xmlFile, string xmlSchemaFile)
        {
            var validationErrors = default(IList<Tuple<object, XmlSchemaException>>);

            try
            {
                Assert.IsTrue(XmlSerialisationHelper.IsValidXml(xmlFile, xmlSchemaFile, out validationErrors));
            }
            catch (AssertionException)
            {
                if (validationErrors != null && validationErrors.Any())
                {
                    foreach (var error in validationErrors)
                        Console.WriteLine("{0}:\t{1}", error.Item1, error.Item2.Message);

                    Console.ReadKey();
                }

                return false;
            }

            return true;
        }

    }
}
