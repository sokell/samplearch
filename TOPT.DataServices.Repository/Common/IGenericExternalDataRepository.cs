﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using TOPT.DataServices.Model.Common;

namespace TOPT.DataServices.Repository.Common
{
    /// <summary>
    /// IExternalDataRepository
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    public interface IGenericExternalDataRepository<TEntity>: IDisposable where TEntity : BaseEntity
    {
        /// <summary>
        /// Size of data
        /// </summary>
        /// <value>
        /// The count.
        /// </value>
        int Size(string data);

        /// <summary>
        /// Gets the data from socket.
        /// </summary>
        /// <returns>The data from socket.</returns>
        /// <param name="endpoint">Endpoint.</param>
        string GetDataFromSocket<T>(string endpoint, T data) where T : BaseEntity, new();
    }
}
