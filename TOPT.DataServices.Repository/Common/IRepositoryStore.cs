﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TOPT.DataServices.Repository.Common
{
    public class ConnectionOptions
    {
        public string DataPathLocation { get; set; }

        public string ConnectionString { get; set; }

        public string Provider { get; set; }

        public int RetryCount { get; set; } = 3;

        public int IntervalRetry { get; set; }

        public Func<Exception, bool> Handles { get; set; }

        public Action FallbackConnection { get; set; }

        public Action<string, object> FallBackAction { get; set; }
    }

    public interface IRepositoryStore<T> : IDisposable where T : class, new()
    {

        string Connect(ConnectionOptions connectionOptions);

        IQueryable<string> ExternalData { get; } //TODO: do we need to work with Xml Docs, or other?

        Task<T> AddAsync(T value);

        Task<T> UpdateAsync(T value);

        Task<T> RemoveAsync(T value);

        Task<IEnumerable<T>> SearchAsync(Func<T, bool> filter);

        Task<T> SearchASingleItemAsync(Func<T, bool> filter);

    }
}
