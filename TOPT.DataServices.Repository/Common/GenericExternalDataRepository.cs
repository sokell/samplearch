﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using log4net.Config;
using Infrastructure.Logging;
using TOPT.DataServices.Model.Common;
using TOPT.DataServices.Utilities;
using TOPT.External.DataHandler;
// ReSharper disable InconsistentNaming

namespace TOPT.DataServices.Repository.Common
{
    /// <summary>
    /// GenericExternalDataRepository of TEntity
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <seealso cref="TOPT.DataServices.Repository.Common.IGenericExternalDataRepository{TEntity}" />
    public class GenericExternalDataRepository<TEntity> : IGenericExternalDataRepository<TEntity> where TEntity : BaseEntity
    {

        //
        // TODO: add ability to open and load from file
        // TODO: add ability to connect, maybe listen, add stream of data
        // TODO: add async + cancellation token
        // TODO: add verification of filename, datapath, size, connection, etc
        //

        private readonly string _endpoint;

        private readonly string _dataPath;

        private readonly string _fileName;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericExternalDataRepository{TEntity}" /> class.
        /// </summary>
        public GenericExternalDataRepository()
        {
            XmlConfigurator.Configure();

            Audit.Log.Info("GenericExternalDataRepository() :: initialized in constructor");
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericExternalDataRepository{TEntity}" /> class.
        /// </summary>
        /// <param name="endpoint">The context.</param>
        public GenericExternalDataRepository(string endpoint)
        {
            try
            {
                _endpoint = endpoint;
                Audit.Log.Info("GenericExternalDataRepository(string endpoint) called ...");
                //TODO: add connect? Listen?
            }
            catch (Exception ex)
            {
                Audit.Log.Error(AppConstant.ErrorMessages.ExceptionMessage, ex);
            }
        }

        protected GenericExternalDataRepository(string dataPath, string fileName)
        {
            _dataPath = dataPath;
            _fileName = fileName;

            //TODO: add action of verification + loading file
        }

        /// <summary>
        /// Count of current db table set
        /// </summary>
        /// <value>
        /// The size.
        /// </value>
        public int Size(string data) {
            return data.Length;
        }

        public string GetDataFromSocket<T>(string endpoint, T data) where T: BaseEntity, new()
        {
            return ExternalDataHandler.GetDataFromSocket(endpoint, data);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);

            Audit.Log.Info("GenericExternalDataRepository Dispose :: CurrentDbContext destroyed");
        }

        /// <summary>
        /// Disposes all external resources.
        /// </summary>
        /// <param name="disposing">The dispose indicator.</param>
        private void Dispose(bool disposing)
        {
            if (!disposing) return;

            //TODO: cleanup any resources
        }
    }
}