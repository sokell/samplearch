﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TOPT.DataServices.Model;
using TOPT.DataServices.Repository.Common;

namespace TOPT.DataServices.Repository
{
    public class ExternalDataRepository : GenericExternalDataRepository<ExternalData>, IExternalDataRepository
    {
        //add- here -anything 'custom' - not in GenericExternalDataRepository

        public ExternalDataRepository(){}

        public ExternalDataRepository(string endpoint) : base(endpoint)
        {
        }

        protected ExternalDataRepository(string dataPath, string fileName) : base(dataPath, fileName)
        {
        }
    }
}
