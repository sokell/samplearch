﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TOPT.DataServices.Model;
using TOPT.DataServices.Repository.Common;

namespace TOPT.DataServices.Repository
{
    public interface IExternalDataRepository : IGenericExternalDataRepository<ExternalData>
    {
        //add- here -anything 'custom' - not in IGenericExternalDataRepository
    }
}
