﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TOPT.DataServices.Model;
using TOPT.DataServices.Repository.Common;

namespace TOPT.DataServices.Repository
{
    public interface IPersonRepository : IGenericRepository<Person>
    {
        //Person GetById(long id);
    }
}
