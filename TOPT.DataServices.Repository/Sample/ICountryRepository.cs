﻿using System;
using TOPT.DataServices.Model;
using TOPT.DataServices.Repository.Common;

namespace TOPT.DataServices.Repository
{
    public interface ICountryRepository : IGenericRepository<Country>
    {
        
    }
}
