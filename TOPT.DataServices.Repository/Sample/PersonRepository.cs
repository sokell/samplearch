﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TOPT.DataServices.Model;
using TOPT.DataServices.Repository.Common;

namespace TOPT.DataServices.Repository
{
    /// <summary>
    /// Sample PersonRepository
    /// <example>
    /// 
    /// Override usage:
    ///public override IEnumerable<Person> GetAll()
    ///{
    ///    return _currentDbContext.Set<Person>().Include(x=>x.Country).AsEnumerable(); 
    ///}

    ///public Person GetById(long id)
    ///{
    ///    return _currentDbSet.Include(x=>x.Country).FirstOrDefault(x => x.Id == id);            
    ///} 
    /// 
    /// </example>
    /// </summary>
    public class PersonRepository : GenericRepository<Person>, IPersonRepository
    {
        public PersonRepository(DbContext context)
            : base(context)
        {
            //   
        }
    }
}
