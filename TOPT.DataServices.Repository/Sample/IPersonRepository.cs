﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TOPT.DataServices.Model;
using TOPT.DataServices.Repository.Common;

namespace TOPT.DataServices.Repository
{
    /// <summary>
    /// IPersonRepository
    /// 
    /// <example>
    /// 
    /// Usage example, additional interface behaviors:
    /// 
    /// Person GetById(long id);
    /// 
    /// </example>
    /// </summary>
    public interface IPersonRepository : IGenericRepository<Person>
    {
        
    }
}
